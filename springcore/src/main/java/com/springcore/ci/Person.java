package com.springcore.ci;

public class Person {
	private String name;
	private int personId;
	Certi certi;
	public Person(String name, int personId,Certi certi) {
		super();
		this.name = name;
		this.personId = personId;
		this.certi = certi;
	}
	@Override
	public String toString() {
		return name + " " + personId + "{ " + certi +"}";
	}
	
	

}
